###Laravel + GitLab + Envoy Deployment Script

This repo is used to deploy php applications with ci/cd of Gitlab. 

Is based in this tutorial: https://docs.gitlab.com/ee/articles/laravel_with_gitlab_and_envoy/

Contains:

* Laravel Envoy script (https://laravel.com/docs/5.5/envoy).
* Dockerfile to build the deploy image.
* Gitlab yml to execute the deployment.
